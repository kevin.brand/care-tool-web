/**
 * This is the context switch event model
 */
 function ContextSwitchEvent(context, description) {
    this.context = context;
    this.description = description;
    this.time = '';

    /**
     * Converts this object into a json object
     * 
     * @returns {contextSwitchEventObj} The ContextSwitchEvent as JSON Object
     */
    this.asJSONObject = function() {
        time = BackgroundUtility.getTime();
        let contextSwitchEvent = '{"eventID" : "' + contextSwitchEventID + '", "newContext" : "' + context + '", "description" : "' + description + '", "screenshot" : "' + 'NONE' + '", "time" : "' + time + '"}';
        let contextSwitchEventObj = JSON.parse(contextSwitchEvent);

        return contextSwitchEventObj;
    }

    /**
     * Converts this object into a json object
     * 
     * @returns {contextSwitchEventObj} The ContextSwitchEvent as JSON Object
     */
    this.asJSONObjectWithScreenshot = function(screenshot) {
        time = BackgroundUtility.getTime();
        let contextSwitchEvent = '{"eventID" : "' + contextSwitchEventID + '", "newContext" : "' + context + '", "description" : "' + description + '", "screenshot" : "' + screenshot + '", "time" : "' + time + '"}';
        let contextSwitchEventObj = JSON.parse(contextSwitchEvent);

        return contextSwitchEventObj;
    }
}