/**
 * This script is capturing the current tab and returns a screenshot as base64 encoded jpg.
 */

// Event Listener
chrome.runtime.onMessage.addListener(
    // Event Callback
    function(request, sender, sendResponse) {
        if(request.type === "capture") {
            chrome.tabs.captureVisibleTab (null, {quality : imageQuality},
                // Respondes with the image as base63 encoded jpg.
                function(dataURL) {
                    let img = dataURL.substring(23);
                    sendResponse({imgSrc : img});
                    return true;
                }
            );
        }
        return true;
    }
);