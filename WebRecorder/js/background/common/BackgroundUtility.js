class BackgroundUtility {
    /**
     * Gets the current time
     * 
     * @returns {time} The current time formated as HH:MM:SS
     */
    static getTime () {
        let date = new Date();

        let time =  date.getHours() + ":" +  
                    date.getMinutes() + ":" +
                    date.getSeconds();

        return time;
    }
}