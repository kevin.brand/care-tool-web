/*
* This script is resonsible for sending event data to the web-Receiver.
*/

var eventSender = new EventSender();

function EventSender() {
    let filter = false; 
    
    // This function is the exposed interface to all the background listener classes
    this.receive = function(eventData) {
        if(eventData == null)
            return;

        send(eventData);
    }
    
    // This function is sending the event data to the Web-Receiver
    let send = function(eventData) {
        if (eventData.eventID == contextSwitchEventID) {
            filter = checkFilter(eventData.newContext);
        }

        //console.log('The events are currently being filtered ' + filter);

        // Event Sendig goes here
        // Sending and receiving data in JSON format using POST method
        if(filter == false) {
            //console.log("Sending: " + JSON.stringify(eventData));
            var xhr = new XMLHttpRequest();
            var url = "http://localhost:15578/api/postEvent/" + eventData.eventID;
            xhr.open("POST", url, true);
            xhr.setRequestHeader("Content-Type", "application/json");
            xhr.send(JSON.stringify(eventData));
        }
    }

    let checkFilter = function (context) {

        if (useWebsiteListAsBlacklist) {
            for (var i = 0; i < websiteList.length; i++) {
                if (context.includes(websiteList[i])) {
                    return true; // Filter
                }
            }
    
            return false; // Dont filter
        }
        else {
            for (var i = 0; i < websiteList.length; i++) {
                if (context.includes(websiteList[i])) {
                    return false; // dont filter
                }
            }
    
            return true; // filter
        }
    }
}

// Event Listener for Content-Script Events (This will recieve jsonObj events from the content script)
chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse) {
        if (request.event) {
            eventSender.receive(request.event);
            sendResponse({farewell: "OK"});
        }
    }
);