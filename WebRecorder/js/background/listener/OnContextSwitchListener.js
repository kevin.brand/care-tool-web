/*
* This script is responsonsible for keeping track of the active context.
* If the context changes (be it an active tab change or a new Web-Page) then inform the event sender script.
*/

// Hook into the onHighlighted Event (which is called whenever a tab is selected / created)
chrome.tabs.onHighlighted.addListener(onHighlighted);

// Hook into the onUpdated Event (which is called whenever a website inside of a tab is updated / a newwebsite is loaded)
chrome.tabs.onUpdated.addListener(onUpdated);

const newTabString = "chrome://newtab/";
const highlightedString = "Tab Changed";
const updatedString = "Website Loaded";
let lastLoadedWebsite;

// This function is the callback to the onHighlighted Event
function onHighlighted(tab) {
    chrome.tabs.getSelected(null, function(tab) {
        let tabURL = tab.url;

        // Empty string means new tab
        if (tabURL === "") {
            tabURL = newTabString;
        }

        delegateContext(highlightedString, tabURL);
        lastLoadedWebsite = tabURL;
    });
}

// This function is the callback to the onUpdated Event
function onUpdated() {
    chrome.tabs.getSelected(null, function(tab) {
        tabURL = tab.url;

        // This check prevents the same update to register multiple times
        if(tabURL !== lastLoadedWebsite) {
            delegateContext(updatedString, tabURL);
            lastLoadedWebsite = tabURL;
        }
    });
}

// This function builds the JSON Object and calls the EventSender
function delegateContext(description, context) {
    setTimeout(function() {
        let contextSwitchEvent = new ContextSwitchEvent(context, description);
        
        // Check if we are on a new tab (cant take screenshots there)
        if (context === newTabString) {
            eventSender.receive(contextSwitchEvent.asJSONObject());
        } else {
            // Call to EventSender goes here.
            chrome.tabs.captureVisibleTab (null, {quality : imageQuality},
                // Respondes with the image as base63 encoded jpg.
                function(dataURL) {
                    if(dataURL == null)
                        eventSender.receive(contextSwitchEvent.asJSONObject());
                    else {
                        let img = dataURL.substring(23);
                        eventSender.receive(contextSwitchEvent.asJSONObjectWithScreenshot(img));
                    }
                }
            );
        }
    }, onContextSwitchDelay)

}