/**
 * This scipt is responsible to catch DOM elements that have been clicked on.
 */
 
// Hook into window onlick and delegate the object that has been clicked on
window.onclick = function(e) {
    delegateSelectEvent(e.srcElement);
}

 // This function builds the JSON Object and calls the EventSender
 function delegateSelectEvent(domObj) {
    let selectEvent = new SelectEvent(domObj);

    // Request a screenshot
    chrome.runtime.sendMessage({type: 'capture'}, function(response) {
        if(response == null) {
            chrome.runtime.sendMessage({event: ccpEvent.asJSONObject()});
        } else
            chrome.runtime.sendMessage({event: selectEvent.asJSONObjectWithScreenshot(response.imgSrc)}, function(response) {});
    });
 }