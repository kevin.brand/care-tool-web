/*
* This script is responsible to catch Text Selection Events. 
*/

// Hook into the mouseup event
window.addEventListener('mouseup', checkTextSelection);

// This function checks if text has been selected (on a mouse up event)
// If this is the case, it sends the selected text to the delegate Context function
function checkTextSelection() {
    let selectedText = window.getSelection().toString();

    // JSON doesn't line new lines
    selectedText = selectedText.replace(/(\r\n|\n|\r)/gm, " ");

    // Check if something was selected
    if (selectedText !== '') {
        delegateTextSelectEvent(selectedText);
    }
}

// This function builds the JSON Object and calls the EventSender
function delegateTextSelectEvent(text) {
    let textSelectEvent = new TextSelectEvent(text);

    // Request a screenshot
    chrome.runtime.sendMessage({type: 'capture'}, function(response) {
        if(response == null) {
            chrome.runtime.sendMessage({event: ccpEvent.asJSONObject()});
        } else
            chrome.runtime.sendMessage({event: textSelectEvent.asJSONObjectWithScreenshot(response.imgSrc)}, function(response) {});
    });
}