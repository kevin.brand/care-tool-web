/*
* This script is responsible for listening to mouse events
*/

const leftClick = 'Left Click';
const rightClick = 'Right Click';
const wheelClick = 'Wheel Click';

// Left Click Event
document.body.onmousedown = function(e) {
    onClick(e);
};

// Right Click Event
document.body.oncontextmenu  = function(e) {
    onClick(e);
};

// this function is the callback function for all mouse events
// It recieves the click event and calculates the X, Y Coord. of the mouse
function onClick(click) {
    let x = click.clientX;
    let y = click.clientY;


    // Switch over click type. 1 = Left Click, 2 = Wheel Click (Not Working), 3 = Rightr Click
    let type = '';
    switch(click.which) {
        case 1:
            type = leftClick;
            break;
        case 2:
            type = wheelClick;
            break;
        case 3:
            type = rightClick;
            break;
    }

    // delegateContext call goes here
    delegateMouseEvent(x, y, type);
}

// This function builds the JSON Object and calls the EventSender
function delegateMouseEvent(x, y, clickType) {
    let mouseEvent = new MouseEvent(x, y, clickType);

    // Request a screenshot
    chrome.runtime.sendMessage({type: 'capture'}, function(response) {
        if(response == null) {
            chrome.runtime.sendMessage({event: ccpEvent.asJSONObject()});
        } else
            chrome.runtime.sendMessage({event: mouseEvent.asJSONObjectWithScreenshot(response.imgSrc)}, function(response) {});
    });
}