/**
 * This is the Text Select event prototype
 * 
 * @param {string} text The text of the event
 * @param {*} domElement The dom element the string was typed in
 * */ 

function TextInputEvent(text, domElement) {
    this.text = text;
    this.domElement = domElement;
    this.time = '';

    /**
     * Converts this object into a json object
     * 
     * @returns {textInputEventObj} The TextInputEvent as JSON Object
     */
    this.asJSONObject = function() {
        time = ContentUtility.getTime();

        let htmlString = domElement.outerHTML.replace(/(['"])/g, "\\$1"); 
        let textInputEvent = '{"eventID" : "' + textSelectEventID + '", "text" : "' + text + '", "dom" : "' + htmlString + '", "screenshot" : "' + 'NONE' + '", "time" : "' + time + '"}';
        let textInputEventObj = JSON.parse(textInputEvent);

        return textInputEventObj;
    }

    /**
     * Converts this object into a json object with a screenshot
     * 
     * @returns {textInputEventObj} The TextInputEvent as JSON Object
     */
    this.asJSONObjectWithScreenshot = function(screenshot) {
        time = ContentUtility.getTime();

        let htmlString = domElement.outerHTML.replace(/(['"])/g, "\\$1"); 
        let textInputEvent = '{"eventID" : "' + textInputEventID + '", "text" : "' + text + '", "dom" : "' + htmlString + '", "screenshot" : "' + screenshot + '", "time" : "' + time + '"}';
        let textInputEventObj = JSON.parse(textInputEvent);

        return textInputEventObj;
    }
} 