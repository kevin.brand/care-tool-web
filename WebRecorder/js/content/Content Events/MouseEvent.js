/**
 * This is the Mouse event prototype
 * */ 

function MouseEvent(x, y, clickType) {
    this.x = x;
    this.y = y;
    this.clickType = clickType;
    this.time = '';

    /**
     * Converts this object into a json object
     * 
     * @returns {mouseClickEventObj} The MouseEvent as JSON Object
     */
    this.asJSONObject = function() {
        time = ContentUtility.getTime();
        let mouseClickEvent = '{"eventID" : "' + mouseEventID + '", "xCoordinate" : "' + x + '", "yCoordinate" : "' + y + '", "clickType" : "' + clickType + '", "screenshot" : "' + 'NONE' + '", "time" : "' + time + '"}';
        let mouseClickEventObj = JSON.parse(mouseClickEvent);

        return mouseClickEventObj;
    }

    /**
     * Converts this object into a json object
     * 
     * @returns {mouseClickEventObj} The MouseEvent as JSON Object
     */
    this.asJSONObjectWithScreenshot = function(screenshot) {
        time = ContentUtility.getTime();
        let mouseClickEvent = '{"eventID" : "' + mouseEventID + '", "xCoordinate" : "' + x + '", "yCoordinate" : "' + y + '", "clickType" : "' + clickType + '", "screenshot" : "' + screenshot + '", "time" : "' + time + '"}';
        let mouseClickEventObj = JSON.parse(mouseClickEvent);

        return mouseClickEventObj;
    }
} 