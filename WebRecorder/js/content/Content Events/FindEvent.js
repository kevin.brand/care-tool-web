/**
 * This is the Scroll Event Prototype
 */

function FindEvent() {
    this.time = '';

    /**
     * Converts this object into a json object
     * 
     * @returns {findEventObj} The FindEvent as JSON Object
     */
    this.asJSONObject = function() {
        time = ContentUtility.getTime();

        let findEvent = '{"eventID" : "' + findEventID + '", "screenshot" : "' + 'NONE' + '", "time" : "' + time + '"}';
        let findEventObj = JSON.parse(findEvent);

        return findEventObj;
    }

    /**
     * Converts this object into a json object with an added screenshot
     * 
     * @returns {findEventObj} The FindEvent as JSON Object
     */
    this.asJSONObjectWithScreenshot = function(screenshot) {
        time = ContentUtility.getTime();

        let findEvent = '{"eventID" : "' + findEventID + '", "screenshot" : "' + screenshot + '", "time" : "' + time + '"}';
        let findEventObj = JSON.parse(findEvent);

        return findEventObj;
    }
}