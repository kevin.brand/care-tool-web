/**
 * This is the Text Select event prototype
 * */ 

function TextSelectEvent(text) {
    this.text = text;
    this.time = '';

    /**
     * Converts this object into a json object
     * 
     * @returns {ccpEventObj} The CCPEvent as JSON Object
     */
    this.asJSONObject = function() {
        time = ContentUtility.getTime();

        let textSelectEvent = '{"eventID" : "' + textSelectEventID + '", "text" : "' + text + '", "screenshot" : "' + 'NONE' + '", "time" : "' + time + '"}';
        let textSelectEventObj = JSON.parse(textSelectEvent);

        return textSelectEventObj;
    }

    /**
     * Converts this object into a json object
     * 
     * @returns {ccpEventObj} The CCPEvent as JSON Object
     */
    this.asJSONObjectWithScreenshot = function(screenshot) {
        time = ContentUtility.getTime();

        let textSelectEvent = '{"eventID" : "' + textSelectEventID + '", "text" : "' + text + '", "screenshot" : "' + screenshot + '", "time" : "' + time + '"}';
        let textSelectEventObj = JSON.parse(textSelectEvent);

        return textSelectEventObj;
    }
} 