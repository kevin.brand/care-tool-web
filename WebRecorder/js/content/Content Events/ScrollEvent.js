/**
 * This is the Scroll Event Prototype
 */

function ScrollEvent(xStart, xEnd, yStart, yEnd) {
    this.xStart = xStart;
    this.xEnd = xEnd;
    this.yStart = yStart;
    this.yEnd = yEnd;

    this.time = '';

    /**
     * Converts this object into a json object
     * 
     * @returns {scrollEventObj} The ScrollEvent as JSON Object
     */
    this.asJSONObject = function() {
        time = ContentUtility.getTime();

        let scrollEvent = '{"eventID" : "' + scrollEventID + '", "xStartCoordinate" : "' + xStart + '", "xEndCoordinate" : "' + xEnd + '", "yStartCoordinate" : "' + yStart + '", "yEndCoordinate" : "' + yEnd + '", "screenshot" : "' + 'NONE' + '", "time" : "' + time + '"}';
        let scrollEventObj = JSON.parse(scrollEvent);

        return scrollEventObj;
    }

    /**
     * Converts this object into a json object
     * 
     * @returns {scrollEventObj} The ScrollEvent as JSON Object
     */
    this.asJSONObjectWithScreenshot = function(screenshot) {
        time = ContentUtility.getTime();

        let scrollEvent = '{"eventID" : "' + scrollEventID + '", "xStartCoordinate" : "' + xStart + '", "xEndCoordinate" : "' + xEnd + '", "yStartCoordinate" : "' + yStart + '", "yEndCoordinate" : "' + yEnd + '", "screenshot" : "' + screenshot + '", "time" : "' + time + '"}';
        let scrollEventObj = JSON.parse(scrollEvent);

        return scrollEventObj;
    }
}