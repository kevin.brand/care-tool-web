/**
 * This is the CCP event prototype
 * */

function CCPEvent(type, text) {
    this.type = type;
    this.text = text;
    

    /**
     * Converts this object into a json object with an added screenshot
     * 
     * @returns {ccpEventObj} The CCPEvent as JSON Object
     */
    this.asJSONObjectWithScreenshot = function(screenshot) {
        let time = '';
        time = ContentUtility.getTime();

        let ccpEvent = '{"eventID" : "' + ccpEventID + '", "type" : "' + type + '", "text" : "' + text + '", "screenshot" : "' + screenshot + '", "time" : "' + time + '"}';
        let ccpEventObj = JSON.parse(ccpEvent);

        return ccpEventObj;
    }

    /**
     * Converts this object into a json object without a screenshot. (screenshot parameter is set to NONE)
     * 
     * @returns {ccpEventObj} The CCPEvent as JSON Object
     */
    this.asJSONObject = function() {
        let time = '';
        time = ContentUtility.getTime();

        let ccpEvent = '{"eventID" : "' + ccpEventID + '", "type" : "' + type + '", "text" : "' + text + '", "screenshot" : "' + 'NONE' + '", "time" : "' + time + '"}';
        let ccpEventObj = JSON.parse(ccpEvent);

        return ccpEventObj;
    }
} 