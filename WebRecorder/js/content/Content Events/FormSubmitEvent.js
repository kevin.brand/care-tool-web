/**
 * This is the Form Submit Event prototype
 * */ 

function FormSubmitEvent(submitData) {
    this.submitData = submitData;
    this.time = '';

    /**
     * Converts this object into a json object
     * 
     * @returns {mouseClickEventObj} The MouseEvent as JSON Object
     */
    this.asJSONObject = function() {
        time = ContentUtility.getTime();

        let submitEvent = '{"eventID" : "' + formSubmitEventID + '", "data" : "' + submitData + '", "screenshot" : "' + 'NONE' + '", "time" : "' + time + '"}';
        let submitEventObj = JSON.parse(submitEvent);

        return submitEventObj;
    }

    /**
     * Converts this object into a json object
     * 
     * @returns {mouseClickEventObj} The MouseEvent as JSON Object
     */
    this.asJSONObjectWithScreenshot = function(screenshot) {
        time = ContentUtility.getTime();

        let submitEvent = '{"eventID" : "' + formSubmitEventID + '", "data" : "' + submitData + '", "screenshot" : "' + screenshot + '", "time" : "' + time + '"}';
        let submitEventObj = JSON.parse(submitEvent);

        return submitEventObj;
    }
} 