/**
 * This is the Select event prototype
 * */ 

function SelectEvent(domObj) {
    this.domObj = domObj;
    this.time = '';

    /**
     * Converts this object into a json object
     * 
     * @returns {ccpEventObj} The CCPEvent as JSON Object
     */
    this.asJSONObject = function() {
        time = ContentUtility.getTime();

        let htmlString = domObj.outerHTML.replace(/(['"])/g, "\\$1"); 
        let selectEvent = '{"eventID" : "' + selectEventID + '", "data" : "' + htmlString + '", "screenshot" : "' + 'NONE' + '", "time" : "' + time + '"}';
        let selectEventObj = JSON.parse(selectEvent);

        return selectEventObj;
    }

    /**
     * Converts this object into a json object
     * 
     * @returns {ccpEventObj} The CCPEvent as JSON Object
     */
    this.asJSONObjectWithScreenshot = function(screenshot) {
        time = ContentUtility.getTime();

        let htmlString = domObj.outerHTML.replace(/(['"])/g, "\\$1"); 
        let selectEvent = '{"eventID" : "' + selectEventID + '", "data" : "' + htmlString + '", "screenshot" : "' + screenshot + '", "time" : "' + time + '"}';
        let selectEventObj = JSON.parse(selectEvent);

        return selectEventObj;
    }
} 