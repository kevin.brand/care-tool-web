/**
 * This script is catching the scroll events on a window
 */

let isScrolling;
let yScrollStart = 0;
let xScrollStart = 0;  

// Request settings
chrome.runtime.sendMessage({type: 'settings'}, function(response) {
    let settings = response.settings; 

    // Hook into the scroll event
    window.addEventListener('scroll', function ( event ) { 
        // Clear our timeout throughout the scroll
        window.clearTimeout( isScrolling );
    
        // Set a timeout to run after scrolling ends
        isScrolling = setTimeout(function() {
    
            // Run the callback
            onScroll();
    
        }, settings.scrollEventFireDelay);
    }, false);
});



// Callback function for the scroll event.
// Calculates the yEnd, xEnd 
function onScroll() {
    // Get the current scroll location
    let scrolledX = window.scrollX;
    let scrolledY = window.scrollY;

    //Call the delegation method
    delegateScrollEvent(xScrollStart, scrolledX, yScrollStart, scrolledY);

    // Set the new starting positions
    xScrollStart = scrolledX;
    yScrollStart = scrolledY;
}

/**
 * 
 * @param {int} xStart The X coord from which the scroll was initiated
 * @param {int} xEnd The X coord at which the scroll was stopped
 * @param {int} yStart The Y coord from which the scroll was initiated
 * @param {int} yEnd The Y coord at which the scroll was stopped
 */
function delegateScrollEvent(xStart, xEnd, yStart, yEnd) {
    let scrollEvent = new ScrollEvent(xStart, xEnd, yStart, yEnd);

    // Request a screenshot
    chrome.runtime.sendMessage({type: 'capture'}, function(response) {
        if(response == null) {
            chrome.runtime.sendMessage({event: ccpEvent.asJSONObject()});
        } else
            chrome.runtime.sendMessage({event: scrollEvent.asJSONObjectWithScreenshot(response.imgSrc)}, function(response) {});
    });
}