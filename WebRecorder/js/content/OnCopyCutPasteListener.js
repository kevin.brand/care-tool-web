/**
 * This script is responsible to catch all copy, cut and paste events that the content may fire.
 */

// Hook into the copy event
document.addEventListener('copy', (event) => {
    let selection = document.getSelection();
    let text = selection.toString();

    if(selection)
        delegateCCPEvent('copy', text);
});

// Hook into the copy event
document.addEventListener('cut', (event) => {
    let selection = document.getSelection();
    let text = selection.toString();

    if(selection)
        delegateCCPEvent('cut', text);
});

// Hook into the paste event
document.addEventListener('paste', (event) => {
    let clipboardData = event.clipboardData || window.clipboardData;
    let text = clipboardData.getData('Text');

    if(clipboardData)
        delegateCCPEvent('paste', text);
});

/**
 * Creates the event and sends it to the event sender
 * 
 * @param {string} type "Cut" or "Copy" or "Paste"  
 * @param {string} text The text that was either cut, copied or pasted
 */
function delegateCCPEvent(type, text) {
    let ccpEvent = new CCPEvent(type, text);

    // Request a screenshot
    chrome.runtime.sendMessage({type: 'capture'}, function(response) {
        if(response == null) {
            chrome.runtime.sendMessage({event: ccpEvent.asJSONObject()});
        } else {
            // Send Message to background scripts (Event sender listens to this)
            chrome.runtime.sendMessage({event: ccpEvent.asJSONObjectWithScreenshot(response.imgSrc)});
        }
    });
}

