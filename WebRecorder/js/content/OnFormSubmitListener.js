/**
 * This scipt is responsible to catch submit event.
 * NOTE: This ONLY works with true form submit events.
 */

// Hook into all Form/Submit DOM-Elements to listen for their submits
let submitForms = document.querySelectorAll('form');

// Iterate over all forms and add an event listener
submitForms.forEach(function(submitForm) {
    submitForm.addEventListener('submit', getSubmitData);
});

// This function cateches submit events and extracts the FormData from it
function getSubmitData(event) {
    let formData = new FormData(event.target)
    let formKeys = formData.keys();
    let formEntries = formData.entries();

    let submitData = [];

    while (!formKeys.next().done) {
        submitData.push(formEntries.next().value);
    } 

    delegateSubmitEvent(submitData);
}

// This function builds the JSON Object and calls the EventSender
function delegateSubmitEvent(submitData) {
    let submitEvent = new FormSubmitEvent(submitData);

    // Request a screenshot
    chrome.runtime.sendMessage({type: 'capture'}, function(response) {
        // Send Message to background scripts (Event sender listens to this)
        if(response == null) {
            chrome.runtime.sendMessage({event: ccpEvent.asJSONObject()});
        } else
            chrome.runtime.sendMessage({event: submitEvent.asJSONObjectWithScreenshot(response.imgSrc)}, function(response) {});
    });
}