/**
 * This script is responsible to catch the find event.
 */

window.addEventListener('keydown', checkFind);

function checkFind(e) {
    if (e.keyCode == 70 && e.ctrlKey)
        onFind();
}

function onFind() {
    delegateFindEvent();
}

// This function builds the JSON Object and calls the EventSender
function delegateFindEvent() {
    let findEvent = new FindEvent();

    // Send Message to background scripts (Event sender listens to this)
    // Request a screenshot
    chrome.runtime.sendMessage({type: 'capture'}, function(response) {
        if(response == null) {
            chrome.runtime.sendMessage({event: ccpEvent.asJSONObject()});
        } else {
            // Send Message to background scripts (Event sender listens to this)
            chrome.runtime.sendMessage({event: findEvent.asJSONObjectWithScreenshot(response.imgSrc)}, function(response) {});
        }
    });
}