/**
 * This config file is going to hold ALL const and vars that are needed throughout the programm.
 * NOTE: While var variables can be changed at any time, after a const is changed a reinstall might be required.
 */

// Delays 
var onContextSwitchDelay = 1000; // The delay in ms after which a context switch event is fired
var scrollEventFireDelay = 500; // The delay is in MS after which a scroll is registered


// Settings
var imageQuality = 50; // Image quality. 1 = Bad; 100 = Full quality 
var recordPasswords = false; // Should passwords be recorded?
var websiteList = []; // Blacklist / Whitelist array
var useWebsiteListAsBlacklist = true; // Should we use Blacklist or Whitelist?

// Event Listener
chrome.runtime.onMessage.addListener(
    // Event Callback
    function(request, sender, sendResponse) {
        if(request.type === "settings") {
            let settings = JSON.parse(getSettings());
            let config = {};
            if(settings != null) {
                config = {
                    imageQuality: settings.screenshotQuality,
                    recordPasswords: settings.recordPwd,
                    websiteList: settings.websiteList,
                    useWebsiteListAsBlacklist: settings.blacklist,
                    scrollEventFireDelay: scrollEventFireDelay
                }
            } else {
                config = {
                    imageQuality: imageQuality,
                    recordPasswords: recordPasswords,
                    websiteList: websiteList,
                    useWebsiteListAsBlacklist: useWebsiteListAsBlacklist,
                    scrollEventFireDelay: scrollEventFireDelay
                }
            }

            sendResponse({settings : config});
            return true;
        }
        return true;
    }
);

loadSettings();
function loadSettings() {
    let s = getSettings();

    if (s != null) {
        let settings = JSON.parse(s);
        imageQuality = settings.screenshotQuality;
        recordPasswords = settings.recordPwd;
        websiteList = settings.websiteList;
        useWebsiteListAsBlacklist = settings.blacklist;
    }
}

function getSettings() {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", "http://localhost:15578/api/getConfig", false ); // false for synchronous request
    xmlHttp.send( null );
    return xmlHttp.responseText;
}