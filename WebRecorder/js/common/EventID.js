// Event IDs
const contextSwitchEventID = 1;
const formSubmitEventID = 2;
const mouseEventID = 3; 
const scrollEventID = 5;
const selectEventID = 6;
const ccpEventID = 7; 
const textInputEventID = 9;
const textSelectEventID = 10;
const findEventID = 11;